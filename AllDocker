Consumer:
FROM python:2.7-stretch
# uWSGI:
USER root
VOLUME /data
RUN pip install uwsgi

# Copy the base uWSGI ini file to enable default dynamic uwsgi process number
COPY ./uwsgi.ini /etc/uwsgi/

# Which uWSGI .ini file should be used, to make it customizable
ENV UWSGI_INI /etc/uwsgi/uwsgi.ini

# By default, run 2 processes
ENV UWSGI_CHEAPER 2

# By default, when on demand, run up to 16 processes
ENV UWSGI_PROCESSES 16

# CRON:
USER root
ARG INSTALL_CRON=false
RUN if [ ${INSTALL_CRON} = true ]; then \
    apt-get update -yqq ; \
    apt-get -y install cron \
;fi

# SUPERVISOR:
USER root
ARG INSTALL_SUPERVISOR=false
RUN if [ ${INSTALL_SUPERVISOR} = true ]; then \
    apt-get update -yqq ; \
    apt-get -y install supervisor \
;fi

# NPM:
USER root
ARG INSTALL_NPM=false
ARG NODE_VERSION
RUN if [ ${INSTALL_NPM} = true ]; then \
    apt-get update -yqq ; \
    apt-get install -y gnupg2 ; \
    curl -sL https://deb.nodesource.com/setup_${NODE_VERSION}.x | bash - ; \
    apt-get install nodejs npm ; \
    apt-get -y install nodejs \
;fi

# GIT:
USER root
ARG INSTALL_GIT=false
RUN if [ ${INSTALL_GIT} = true ]; then \
    apt-get -y install git \
;fi

# ZIP:
USER root
ARG INSTALL_ZIP=false
RUN if [ ${INSTALL_ZIP} = true ]; then \
    apt-get -y install zip unzip \
;fi

# Image optimizers:
USER root
ARG INSTALL_IMAGE_OPTIMIZERS=false
RUN if [ ${INSTALL_IMAGE_OPTIMIZERS} = true ]; then \
    apt-get install -y jpegoptim optipng pngquant gifsicle \
;fi

# pgsql client
ARG INSTALL_PG_CLIENT=false

RUN if [ ${INSTALL_PG_CLIENT} = true ]; then \
    # Create folders if not exists (https://github.com/tianon/docker-brew-debian/issues/65)
    mkdir -p /usr/share/man/man1 && \
    mkdir -p /usr/share/man/man7 && \
    # Install the pgsql client
    apt-get install -y postgresql-client \
;fi

COPY ./requirements.txt /opt/
RUN pip install -r /opt/requirements.txt
COPY ./supervisor/supervisord.conf /etc/supervisor/
ARG WORK_DIR=/var/www
RUN usermod -u 1000 www-data
WORKDIR ${WORK_DIR}
CMD [ "/usr/bin/supervisord", "-c", "/etc/supervisor/supervisord.conf" ]
EXPOSE 9000
MySql:
ARG MYSQL_VERSION=latest
FROM mysql:${MYSQL_VERSION}

# Set Timezone

ARG TZ=UTC
ENV TZ ${TZ}
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone && chown -R mysql:root /var/lib/mysql/

COPY my.cnf /etc/mysql/conf.d/my.cnf
CMD ["mysqld"]
EXPOSE 3306
Nginx:
FROM nginx:alpine
COPY nginx.conf /etc/nginx/
ARG CHANGE_SOURCE=false
VOLUME /data
RUN if [ ${CHANGE_SOURCE} = true ]; then \
    # Change application source from dl-cdn.alpinelinux.org to aliyun source
    sed -i 's/dl-cdn.alpinelinux.org/mirrors.aliyun.com/' /etc/apk/repositories \
;fi
#Attach www-data user to an already existing group with -Gy
RUN apk update \
    && apk upgrade \
    && apk add --no-cache openssl \
    && apk add --no-cache bash

RUN if [ `grep -c www-data /etc/group` = 0 ]; then \
         adduser -D -H -u 1000 -s /bin/bash www-data; \
    else \
         adduser -D -H -u 1000 -s /bin/bash www-data -G www-data; \
    fi

ADD ./startup.sh /opt/startup.sh
RUN sed -i 's/\r//g' /opt/startup.sh
CMD ["/bin/bash", "/opt/startup.sh"]
EXPOSE 80 443
Redis:
FROM redis
RUN mkdir -p /etc/redis
COPY redis.conf /etc/redis/redis.conf
VOLUME /data
CMD ["redis-server"]
EXPOSE 6379
Web:
FROM python:2.7-stretch
USER root
VOLUME /data
# uWSGI:
RUN pip install uwsgi
# Copy the base uWSGI ini file to enable default dynamic uwsgi process number
COPY ./uwsgi.ini /etc/uwsgi/
# Which uWSGI .ini file should be used, to make it customizable
ENV UWSGI_INI /etc/uwsgi/uwsgi.ini
# By default, run 2 processes
ENV UWSGI_CHEAPER 2
# By default, when on demand, run up to 16 processes
ENV UWSGI_PROCESSES 16
# CRON:
USER root
ARG INSTALL_CRON=false
RUN if [ ${INSTALL_CRON} = true ]; then \
    apt-get update -yqq ; \
    apt-get -y install cron \
;fi
# SUPERVISOR:
USER root
ARG INSTALL_SUPERVISOR=false
RUN if [ ${INSTALL_SUPERVISOR} = true ]; then \
    apt-get update -yqq ; \
    apt-get -y install supervisor \
;fi

# NPM:
USER root
ARG INSTALL_NPM=false
ARG NODE_VERSION
RUN if [ ${INSTALL_NPM} = true ]; then \
    apt-get update -yqq ; \
    apt-get install -y gnupg2 ; \
    curl -sL https://deb.nodesource.com/setup_${NODE_VERSION}.x | bash - ; \
    apt-get install nodejs npm ; \
    apt-get -y install nodejs \
;fi

# GIT:
USER root
ARG INSTALL_GIT=false
RUN if [ ${INSTALL_GIT} = true ]; then \
    apt-get -y install git \
;fi
# ZIP:
USER root
ARG INSTALL_ZIP=false
RUN if [ ${INSTALL_ZIP} = true ]; then \
    apt-get -y install zip unzip \
;fi

# Image optimizers:
USER root
ARG INSTALL_IMAGE_OPTIMIZERS=false
RUN if [ ${INSTALL_IMAGE_OPTIMIZERS} = true ]; then \
    apt-get install -y jpegoptim optipng pngquant gifsicle \
;fi
# pgsql client
ARG INSTALL_PG_CLIENT=false
RUN if [ ${INSTALL_PG_CLIENT} = true ]; then \
    # Create folders if not exists (https://github.com/tianon/docker-brew-debian/issues/65)
    mkdir -p /usr/share/man/man1 && \
    mkdir -p /usr/share/man/man7 && \
    # Install the pgsql client
    apt-get install -y postgresql-client \
;fi

COPY ./requirements.txt /opt/
RUN pip install -r /opt/requirements.txt
ARG WORK_DIR=/var/www
RUN usermod -u 1000 www-data
WORKDIR ${WORK_DIR}
CMD [ "uwsgi" ]
EXPOSE 9000
